export BERT_VOCAB="bert-base-multilingual-cased"
export BERT_WEIGHTS="bert-base-multilingual-cased"
export GOLD_BASE="/moredata/muller/Linagora/noisy_annotation_snorkel/"
export DATASET="fr.linto"
export CONFIG="linto.snorkel"
export OUTPUT=${DATASET}"_bertM"
export MODEL="bertM"
export TEST_A_PATH=$GOLD_BASE/file_gold_dev_210107.txt_cut 
# if fine-tune model, different path. set to "" if no fine-tuning
export FINE_TUNE="_ft"

export CONFIG=${CONFIG}${FINE_TUNE}
#python conv2ner.py --split-too-long True 200  --input-format ner $TEST_A_PATH > $TEST_DATA_PATH"_cut" 
#allennlp train -s Results_${CONFIG}/results_${OUTPUT} configs/bert.jsonnet
# predict with model -> outputs json
allennlp predict --use-dataset-reader --output-file Results_${CONFIG}/results_${OUTPUT}/${DATASET}_${EVAL}.predictions.json Results_${CONFIG}/results_${OUTPUT}/model.tar.gz ${TEST_A_PATH}
# convert to disrpt format 
python json2conll.py Results_${CONFIG}/results_${OUTPUT}/${DATASET}_${EVAL}.predictions.json ${CONFIG} > Results_${CONFIG}/results_${OUTPUT}/${DATASET}_${EVAL}.predictions.${CONFIG}
# eval with disrpt script
export GOLD=$TEST_A_PATH
python ner2disrpt.py $GOLD > $GOLD.disrpt
python ../utils/seg_eval.py $GOLD.disrpt Results_${CONFIG}/results_${OUTPUT}/${DATASET}_${EVAL}.predictions.${CONFIG} >> Results_${CONFIG}/results_${OUTPUT}/${DATASET}_${EVAL}.scores
