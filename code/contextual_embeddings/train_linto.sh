export BERT_VOCAB="bert-base-multilingual-cased"
export BERT_WEIGHTS="bert-base-multilingual-cased"
export GOLD_BASE="/moredata/muller/Linagora/noisy_annotation_snorkel/"
export DATASET="fr.linto"
export CONFIG="linto.snorkel"
export TRAIN_DATA_PATH=$GOLD_BASE/file_train_tony_210107.txt 
python conv2ner.py --split-too-long True 200  --input-format ner $TRAIN_DATA_PATH > $TRAIN_DATA_PATH"_cut" 
export TRAIN_DATA_PATH=$TRAIN_DATA_PATH"_cut"
export OUTPUT=${DATASET}"_bertM"
export MODEL="bertM"
export TEST_A_PATH=$GOLD_BASE/file_gold_dev_210107.txt
python conv2ner.py --split-too-long True 200  --input-format ner $TEST_A_PATH > $TEST_A_PATH"_cut" 
export TEST_A_PATH=$TEST_A_PATH"_cut"
allennlp train -s Results_${CONFIG}/results_${OUTPUT} configs/bert.jsonnet
